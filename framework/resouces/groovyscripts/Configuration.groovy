import static Util.*
import static groovy.io.FileType.*

class Configuration
{
    def separator = get_path_separator()
    def config_dir = convert_to_os_path(get_attribute("framework.config.dir"))
    def dbdrivers_dir = convert_to_os_path(get_attribute("framework.dbdrivers.dir"))
    def template_dir = new File("$config_dir${separator}template")
    def environment = get_attribute("build.environment")
    def hybris_config_dir = convert_to_os_path(get_attribute("hybris.config.dir", "hybris/config"))
    def hybris_db_dir = convert_to_os_path(get_attribute("hybris.db.dir", "hybris/bin/platform/lib/dbdriver"))
    def platformhome = get_attribute("hybris.home.zip")

    def config()
    {
        ant.delete(dir: "${hybris_config_dir}", failonerror: false)
        ant.mkdir(dir: "${hybris_config_dir}")

        ant.copy(todir: "${hybris_db_dir}", overwrite: true, failonerror: true) {
            fileset(dir: "${dbdrivers_dir}", includes: "**/*")
        }
        ant.copy(todir: "${hybris_config_dir}", overwrite: true, failonerror: false) {
            fileset(dir: "config${separator}template", includes: "**/*")
        }


        ant.copy(todir: "${hybris_config_dir}", overwrite: true, failonerror: false) {
            fileset(file: "${config_dir}${separator}${environment}${separator}local.properties")
            fileset(file: "${config_dir}${separator}${environment}${separator}localextensions.xml")
            fileset(file: "${config_dir}${separator}${environment}${separator}/tomcat/*")
        }

        if (environment == "local")
        {
            ant.concat(destfile: "${hybris_config_dir}${separator}local.properties", append: true) {
                fileset(dir: "$config_dir${separator}local", includes: "user.properties")
            }
        }

        add_release_information()
    }


    void add_release_information()
    {
         def local_properties_file = new File("${hybris_config_dir}${separator}local.properties")
        if(local_properties_file.exists())
        {
            def date = new Date()

            ant.exec(outputproperty: "cmdOut",
                errorproperty: "cmdErr",
                resultproperty: "cmdExit",
                failonerror: "true",
                dir: ".",
                executable: "git") { arg(line: "name-rev --tags --name-only \$(git rev-parse HEAD") }

        def version = ant.project.properties.cmdOut

            local_properties_file.append("\n\n## Build information ##")
            local_properties_file.append("\nbuild.custom.version=${version}")
            local_properties_file.append("\nbuild.custom.date=${date}")
            local_properties_file.append("\nenv=${environment}")

        }
    }

    def setup()
    {
        setup_template_dir()
        config()

    }

    void setup_ignore()
    {
        def ignoreFile = new File(".gitignore")
        if (!ignoreFile.exists())
        {
            ant.echo "Setting up .gitignore"
            ant.copy(file: new File("build${separator}templates${separator}.gitignore"), toFile: ".gitignore")
        }
        else
        {
            ant.echo "ignore already in place."
        }
    }

    void setup_template_dir()
    {
        def path_to_hybris_template_dir = "hybris/bin/platform/resources/configtemplates/develop"
        def hybris_template_dir = new File(convert_to_os_path(path_to_hybris_template_dir))
        ant.copy(todir: template_dir) { fileset(dir: hybris_template_dir) }
    }


}
