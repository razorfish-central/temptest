extensions_dir = args[0]
extensions_raw = args[1] as String
extensions = extensions_raw.split(",")
output_report_dir = properties["findbugs.dir.reports"] as String
findbugs_home = properties["findbugs.home"]
basedir = properties["basedir"]
def jarFinder
def jarFiles = []

jarFinder = {
  def matchingFiles = []
  it.eachDir(jarFinder);
  it.eachFileMatch(~/.*\.jar$/) {
    jarFiles << it.canonicalPath
  }
}
jarFinder(new File("${basedir}/config/template/licence"))
jarFinder(new File("${basedir}/hybris/bin"))
jarFinder(new File(extensions_dir))

ant.findbugs(home:"${findbugs_home}", output:"xml:withMessages", outputFile:"${output_report_dir}/findbugs.xml", jvmArgs:"-Xmx512m") {
  jarFiles.each() {auxClasspath(path:"${it}")}
  extensions.each() {
    sourcePath(path:"${extensions_dir}/${it}/src")
    ant.class(location:"${extensions_dir}/${it}/classes")
  }
}

