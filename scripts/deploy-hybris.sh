#!/bin/bash

cluster_id=0
user="ec2-user"
env="rf-ci"

home=`awk -F":" '{ print $6 }' /etc/passwd | grep $user\$`

/etc/init.d/hybris stop
echo "Waiting for Hybris server to stop..."
sleep 15
cd /opt
rm -rf hybris/bin
rm -rf hybris/config/*
rm -rf hybris/temp/hybris/*
unzip $home/hybrisServer-Platform.zip
unzip $home/hybrisServer-AllExtensions.zip
unzip -o $home/hybrisServer-Config-$env.zip -d hybris/config/
echo -e "\r\ncluster.id=$cluster_id" >> hybris/config/local.properties
chown -R  hybris:hybrisgrp /opt/hybris
chmod +x /opt/hybris/bin/platform/hybrisserver.sh
chmod +x /opt/hybris/bin/platform/tomcat/bin/catalina.sh
chmod +x /opt/hybris/bin/platform/tomcat/bin/wrapper.sh
cd /opt/hybris/bin/platform
su hybris -c 'ant'
